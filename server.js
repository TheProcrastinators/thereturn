const express = require('express')
const app = express()

app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));

app.set('view engine', 'ejs')

app.get('/', (req, res)=> {
	console.log('here')
	res.render('index', {text:"hello"} )
})

app.listen(8080)
